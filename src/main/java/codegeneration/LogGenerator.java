package codegeneration;

import javax0.geci.tools.AbstractJavaGenerator;
import javax0.geci.tools.CompoundParams;

@RegisterCodeGenerator
public class LogGenerator extends AbstractJavaGenerator {

	public static final String GENERATOR_NAME = "LogGenerator";

	@Override
	public void process(javax0.geci.api.Source source, Class<?> aClass, CompoundParams compoundParams) throws Exception {
		try(var segment = source.open(GENERATOR_NAME)){
			segment.write_r("private static String logRandom(){");
			segment.write("return \"random\";");
			segment.write_l("}");
		}
	}

	@Override
	public String mnemonic() {
		return GENERATOR_NAME;
	}

}
