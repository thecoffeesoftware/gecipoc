package codegeneration;

import javax0.geci.api.Source;
import javax0.geci.tools.AbstractGeneratorEx;
import javax0.geci.tools.JavaSource;
import javax0.geci.tools.Template;

import java.util.HashMap;
import java.util.Map;

@RegisterFileGenerator
public class SampleClassGenerator extends AbstractGeneratorEx {


	@Override
	public void processEx(javax0.geci.api.Source source) {
		createSampleClassWithTemplate(source);
		createSampleClassWithJavaSource(source);
	}

	private void createSampleClassWithJavaSource(Source source) {
		final String CLASS_NAME = "SampleClassGeneratedWithJavaSource";
		final String FILE_NAME = CLASS_NAME + ".java";
		try (var segment = source.newSource("../" + FILE_NAME).open()) {
			Map<String, String> params = new HashMap<>();
			params.put("className", CLASS_NAME);
			segment.write(JavaSource.builder()
					.comment("This class was built with JavaSouce!").toString());

			segment.write_r("public class " + CLASS_NAME + " {\n");
			try (JavaSource.Builder methodBuilder = JavaSource.builder()) {
				try (JavaSource.Uguc method = methodBuilder.method("doRandomThings")) {
					try (JavaSource.MethodBody methodBody = method.modifiers("public")
							.returnType("void")
							.args("boolean isUseless")
							.comment("This is a totally useless method!")
							.newline()) {
						segment.write(methodBody.toString());
						try (JavaSource.Builder builder = JavaSource.builder()) {
							try (JavaSource.Ajef elseStatement = builder.ifStatement("1 == 1")
									.comment("This is always true.")
									.statement("System.out.println(\"Hey\")")
									.elseStatement()) {
								elseStatement.comment("This cannot be reached!");
							}
							segment.write(builder.toString());
						}
						segment.write(JavaSource.builder().statement("return")
								.write("//comment here").toString());
					}
				}
				segment.write_l("}");
				segment.write_l("}");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void createSampleClassWithTemplate(Source source) {
		final String CLASS_NAME = "SampleClassGeneratedWithTemplate";
		final String FILE_NAME = CLASS_NAME + ".java";
		try (var segment = source.newSource("../" + FILE_NAME).open()) {
			Map<String, String> params = new HashMap<>();
			params.put("className", CLASS_NAME);
			Template template = new Template(params);
			String classString = "public class {{className}} {\n" +
					"\n" +
					"	public {{className}}() {\n" +
					"		\n" +
					"	}\n" +
					"}";
			segment.write(template.resolve(classString));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


}
