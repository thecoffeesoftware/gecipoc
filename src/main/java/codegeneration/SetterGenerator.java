package codegeneration;

import javax0.geci.tools.AbstractJavaGenerator;
import javax0.geci.tools.CompoundParams;

import java.lang.reflect.Field;
import java.util.Arrays;

@RegisterCodeGenerator
public class SetterGenerator extends AbstractJavaGenerator {

	public static final String GENERATOR_NAME = "SetterGenerator";

	@Override
	public void process(javax0.geci.api.Source source, Class<?> aClass, CompoundParams compoundParams) throws Exception {
		try(var segment = source.open(GENERATOR_NAME)){
			for (Field declaredField : aClass.getDeclaredFields()) {
				if(!declaredField.isAnnotationPresent(NoSetterGeneration.class)) {
					String fieldName = declaredField.getName();
					String fieldClass = declaredField.getType().getName();
					segment.write_r("public void set" + Util.capitalizeFirstLetter(fieldName) + "(" + fieldClass + " " + fieldName + " ){");
					segment.write("this." + fieldName + " = " + fieldName + ";");
					segment.write_l("}");
				}
			}
		}
	}

	@Override
	public String mnemonic() {
		return GENERATOR_NAME;
	}
}
