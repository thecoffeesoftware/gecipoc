package codegeneration;

import javax0.geci.tools.AbstractJavaGenerator;
import javax0.geci.tools.CompoundParams;

import java.lang.reflect.Field;

@RegisterCodeGenerator
public class GetterGenerator extends AbstractJavaGenerator {

	public static final String GENERATOR_NAME = "GetterGenerator";

	@Override
	public void process(javax0.geci.api.Source source, Class<?> aClass, CompoundParams compoundParams) throws Exception {
		try(var segment = source.open(GENERATOR_NAME)){
			for (Field declaredField : aClass.getDeclaredFields()) {
				String fieldName = declaredField.getName();
				String fieldClass = declaredField.getType().getName();
				segment.write_r("public " + fieldClass + " get" + Util.capitalizeFirstLetter(fieldName) + "(){");
				segment.write("return " + fieldName + ";");
				segment.write_l("}");
			}
		}
	}

	@Override
	public String mnemonic() {
		return GENERATOR_NAME;
	}
}
