import codegeneration.GetterGenerator;
import codegeneration.NoSetterGeneration;
import codegeneration.SetterGenerator;
import javax0.geci.annotations.Geci;
import javax0.geci.annotations.Gecis;

@Gecis({
		@Geci(value = GetterGenerator.GENERATOR_NAME),
		@Geci(value = SetterGenerator.GENERATOR_NAME)
})
public class ClassWithGettersAndSetters {

	private String name = "name";

	@NoSetterGeneration
	private String message = "message";

	private Integer number = 42;


    //<editor-fold id="GetterGenerator">
    public java.lang.String getName(){
        return name;
    }
    public java.lang.String getMessage(){
        return message;
    }
    public java.lang.Integer getNumber(){
        return number;
    }
    //</editor-fold>
    //<editor-fold id="SetterGenerator">
    public void setName(java.lang.String name ){
        this.name = name;
    }
    public void setNumber(java.lang.Integer number ){
        this.number = number;
    }
    //</editor-fold>
}
