package codegeneration;

import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class GeneratorTest {

	@Test
	public void generateCode() {
		if (isCodeGenerationEnabled()) {
			CodeGeneratorManager.enable();
		}
		CodeGeneratorManager.generateCode();
	}


	private boolean isCodeGenerationEnabled() {
		try (InputStream input = new FileInputStream("./src/main/resources/application.properties")) {
			Properties prop = new Properties();
			prop.load(input);
			return Boolean.parseBoolean(prop.getProperty("geci.code-generation.enabled"));
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
