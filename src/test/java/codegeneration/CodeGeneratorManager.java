package codegeneration;

import javax0.geci.api.Generator;
import javax0.geci.engine.Geci;
import javax0.geci.engine.Source;
import javax0.geci.tools.GeciAnnotationTools;
import javax0.geci.tools.GeciReflectionTools;
import org.reflections.Reflections;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class CodeGeneratorManager {

	private static final String BASE_PATH = "src/main/java";

	private static List<Generator> codeGenerators = new ArrayList<>();
	private static List<Generator> fileGenerators = new ArrayList<>();
	private static boolean enabled = false;
	private static boolean generated = false;

	private CodeGeneratorManager() {

	}

	public static void enable() {
		if (!generated) {
			registerGenerators();
		}
		enabled = true;
	}

	public static void disable() {
		enabled = false;
	}

	public static void generateCode() {
		if (enabled) {
			startCodeGenerators();
			startFileGenerators();
		}
	}

	private static void startCodeGenerators(){
		codeGenerators.forEach(codeGenerator -> {
			try {
				new Geci().source(BASE_PATH).register(codeGenerator).generate();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	private static void startFileGenerators(){
		fileGenerators.forEach(fileGenerator -> {
			try {
				new Geci().source(
						BASE_PATH + "/" +
								fileGenerator.getClass()
										.getPackageName()
										.replace(".", "/"))
						.only(fileGenerator.getClass()
								.getSimpleName())
						.register(fileGenerator)
						.source("../")
						.generate();
			} catch (IOException e) {
				e.printStackTrace();
			}
		});
	}

	private static void registerGenerators() {
		Reflections reflections = new Reflections("");
		registerCodeGenerators(reflections);
		registerFileGenerators(reflections);
		generated = true;
	}

	private static void registerCodeGenerators(Reflections reflections){
		Set<Class<?>> classes = reflections.getTypesAnnotatedWith(RegisterCodeGenerator.class);
		classes.forEach(clazz ->{
			if(Generator.class.isAssignableFrom(clazz)){
				try {
					codeGenerators.add((Generator) clazz.getDeclaredConstructor().newInstance());
				} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		});
	}

	private static void registerFileGenerators(Reflections reflections){
		Set<Class<?>> classes = reflections.getTypesAnnotatedWith(RegisterFileGenerator.class);
		classes.forEach(clazz ->{
			if(Generator.class.isAssignableFrom(clazz)){
				try {
					fileGenerators.add((Generator) clazz.getDeclaredConstructor().newInstance());
				} catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
					e.printStackTrace();
				}
			}
		});
	}

}
